# XDrawChem

This is a fork of the [original xdrawchem project][http://xdrawchem.sourceforge.net/] that fixes many bugs and adds some convenience features. See HISTORY.txt for details.

Requires OpenBabel 3 and Qt5 (5.6 or later, again get all devel packages!)


## Building from source
```
$ qmake
$ make
```
To install after building from source, also run:
```
$ make install
```

