<pre>
Contains non-Free header file: CDXConstants.h
Contains non-Free code: goldman_rotate.cpp

Portions copyright by Christoph Steinbeck, the JChemPaint project and
others. All source files contain copyright and licensing information.

-- XDrawChem copyright begins here --

XDrawChem
Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

(Author's note: the license is in the file LICENSE.txt, which is included in the source directory.)
</pre>
