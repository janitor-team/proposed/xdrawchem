// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef BOND_EDIT_DIALOG_H
#define BOND_EDIT_DIALOG_H

#include <QColor>
#include <QComboBox>
#include <QDebug>
#include <QDialog>
#include <QPushButton>

#include "colorbutton.h"
#include "dpoint.h"
#include "previewwidget.h"

class BondEditDialog : public QDialog {
    Q_OBJECT

  public:
    BondEditDialog(QWidget *parent, DPoint *s, DPoint *e, int ty, int o, int d, int th, int st,
                   QColor c1);
    int Style() { return style; }
    int Order() { return order; }
    int Dash() { return dash; }
    QColor Color() { return color; }
    int Thick() { return thick; }
    int DoubleBond() { return dbond; }

  public slots:
    void GetNewColor();

    void styleChanged(int s1) {
        style = s1;
        pw->updateWidget(type, thick, dash, order, style, color);
    }

    void orderChanged(int o1) {
        if (o1 == 0) {
            order = 1;
            dash = 1;
        }
        if (o1 == 1) {
            order = 1;
            dash = 0;
        }
        if (o1 == 2) {
            order = 2;
            dash = 0;
        }
        if (o1 == 3) {
            order = 3;
            dash = 0;
        }
        if (o1 == 5) {
            order = 5;
            dash = 0;
        }
        if (o1 == 6) {
            order = 6;
            dash = 0;
        }
        if (o1 == 7) {
            order = 7;
            dash = 0;
        }
        if (o1 == 11) {
            order = 2;
            dash = 1;
        }
        if (o1 == 12) {
            order = 3;
            dash = 1;
        }
        pw->updateWidget(type, thick, dash, order, style, color);
    }

    void setThick(int tnew) {
        qDebug() << "setThick: " << tnew;
        thick = tnew + 1;
        pw->updateWidget(type, thick, dash, order, style, color);
    }

    void setDoubleBond(int tnew) {
        dbond = tnew;
        dbList->setCurrentIndex(dbond);
    }

  private:
    ColorButton *colorBtn;
    QComboBox *ltList, *dbList;
    DPoint *s1, *e1;
    QColor color;
    int type;
    int order, dash, thick;
    int ahead, style, dbond;
    PreviewWidget *pw;
};

#endif
