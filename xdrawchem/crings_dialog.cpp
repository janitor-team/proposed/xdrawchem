// XDrawChem
// Copyright (C) 2002  Adam Tenderholt <atenderholt@users.sourceforge.net>
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Dialog for Custom Rings

#include <QGridLayout>
#include <QPainter>
#include <QSpacerItem>

#include "crings_dialog.h"
#include "defs.h"
#include "prefs.h"

extern Preferences preferences;

CustomRingWidget::CustomRingWidget(QWidget *parent) : QWidget(parent) {
    QGridLayout *layout = new QGridLayout();
    setLayout(layout);
    layout->setMargin(0);

    setMinimumSize(350, 50);

    render = new Render2D();
    data = new ChemData(this);

    render->setChemData(data);
    data->setRender2D(render);

    label = new QLabel(tr("Title: "));
    layout->addWidget(label, 1, 0, 1, 1);

    title = new QLineEdit();
    layout->addWidget(title, 1, 1, 1, 1);

    connect(title, SIGNAL(textChanged(const QString &)), this,
            SIGNAL(signalTextChanged(const QString &)));

    // render->setMode_SelectNone();
}

CustomRingWidget::~CustomRingWidget() {

    if (render)
        delete render;

    if (data)
        delete data;

    if (title)
        delete title;

    if (label)
        delete label;
}

void CustomRingWidget::slotTitle(QString &string) {

    data->save(string);
    qDebug() << "saved: " << string << Qt::endl << "done with crwidget";
    string.truncate(string.length() - 4);
    string.append(".png");

    if (pixmap.save(string, "PNG"))
      qDebug() << "png: " << string;
}

void CustomRingWidget::setMolecule(Molecule *m) {
    data->addMolecule(m);
    Render2D *real_render = m->getRender2D();


    QPixmap mol;
    QRect rr1 = m->BoundingBoxAll();

    real_render->setMode_Select();
    real_render->DeselectAll();
    pixmap = real_render->grab(rr1);
}

CustomRingDialog::CustomRingDialog(QWidget *parent) : QDialog(parent) {
    setWindowTitle(tr("Add custom ring to menu list"));

    QGridLayout *mygrid = new QGridLayout(this);

    m_widget = new CustomRingWidget(this);
    mygrid->addWidget(m_widget, 0, 0, 1, 3);

    connect(m_widget, SIGNAL(signalTextChanged(const QString &)), this,
            SLOT(slotTextChanged(const QString &)));

    connect(this, SIGNAL(signalTitle(QString &)), m_widget, SLOT(slotTitle(QString &)));

    // disableResize();
    // enableButtonOK(false);

    QSpacerItem *spacer = new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum);

    mygrid->addItem(spacer, 2, 0);

    QPushButton *ok, *dismiss;

    ok = new QPushButton(tr("OK"), this);
    connect(ok, SIGNAL(clicked()), SLOT(slotOk()));
    mygrid->addWidget(ok, 2, 1);

    dismiss = new QPushButton(tr("Cancel"), this);
    connect(dismiss, SIGNAL(clicked()), SLOT(reject()));
    mygrid->addWidget(dismiss, 2, 2);
}

CustomRingDialog::~CustomRingDialog() {

    if (m_widget)
        delete m_widget;
}

void CustomRingDialog::slotTextChanged(const QString &string) {

    // if(string.isEmpty())
    //  enableButtonOK(false);
    // else //if string!=QString::null
    //  enableButtonOK(true);
}

void CustomRingDialog::slotOk() {

    QString title = m_widget->getTitle();

    qDebug() << title;
    QString filename = title.toLower();

    filename.append(".cml");

    QString file = preferences.getCustomRingDir();

    file += filename;

    emit signalTitle(file);

    accept();
}

// cmake#include "crings_dialog.moc"
