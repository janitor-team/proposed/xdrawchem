// XDrawChem
// Copyright (C) 2002  Adam Tenderholt <atenderholt@users.sourceforge.net>
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Dialog for Custom Rings

#ifndef _CRINGS_DIALOG_H
#define _CRINGS_DIALOG_H

#include <QDialog>
#include <QFrame>
#include <QLabel>
#include <QLineEdit>
#include <QPixmap>

#include "chemdata.h"
#include "molecule.h"
#include "render2d.h"

class CustomRingWidget : public QWidget {
    Q_OBJECT

  public:
    CustomRingWidget(QWidget *parent = 0);
    ~CustomRingWidget();

    QString getTitle() { return title->text(); }
    void setMolecule(Molecule *m);

  public slots:
    void slotTitle(QString &string);

  signals:
    void signalTextChanged(const QString &);

  private:
    Render2D *render;
    ChemData *data;
    QLineEdit *title;
    QLabel *label;
    QPixmap pixmap;
};

class CustomRingDialog : public QDialog {
    Q_OBJECT

  public:
    CustomRingDialog(QWidget *parent = 0);
    ~CustomRingDialog();

    void setMolecule(Molecule *m) { m_widget->setMolecule(m); }

  public slots:
    void slotOk();
    void slotTextChanged(const QString &string);

  signals:
    void signalTitle(QString &);

  private:
    CustomRingWidget *m_widget;
};

#endif
