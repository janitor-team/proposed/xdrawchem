// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FIXEDDIALOG_H
#define FIXEDDIALOG_H

#include <QCheckBox>
#include <QDialog>

class QComboBox;
class QDoubleSpinBox;

class FixedDialog : public QDialog {
    Q_OBJECT

  public:
    FixedDialog(QWidget *parent);
    double getLength_bond();
    double getAngle_bond();
    void setLength_bond(double);
    void setAngle_bond(double);
    double getLength_arrow();
    double getAngle_arrow();
    void setLength_arrow(double);
    void setAngle_arrow(double);
    double getDoubleBondOffset();
    void setDoubleBondOffset(double);
    void setAntiChecked(bool checked) { anti_toggle->setChecked(checked); }
    bool getAntiChecked() { return anti_toggle->isChecked(); }

  public slots:
    void onSuccess();
    void setDefaults();
    void setUnits(int);

  private:
    QComboBox *unitCombo;
    QDoubleSpinBox *bondLengthBox;
    QDoubleSpinBox *bondAngleBox;
    QDoubleSpinBox *arrowLengthBox;
    QDoubleSpinBox *arrowAngleBox;
    QDoubleSpinBox *doubleBondOffset;
    QDoubleSpinBox *gridSpacingBox;
    QCheckBox *anti_toggle, *snapGridBox;
    QComboBox *showgrid;
    int bl, al, dsp, gsp, prevunit;
};

#endif
