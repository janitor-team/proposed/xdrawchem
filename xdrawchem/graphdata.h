// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef GRAPHDATA_H
#define GRAPHDATA_H

#include <QColor>

class GraphData {
  public:
    double value;
    QString label;
    QString fulltext;
    int intensity;
    bool drawlabel;
    QColor pcolor;
    GraphData() {
        drawlabel = false;
        intensity = 1;
        pcolor = QColor(0, 0, 0);
    }
};

#endif
