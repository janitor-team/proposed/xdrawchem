// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// molecule_smiles.cpp - Molecule's implementation of even more functions,
// notably, Structural Diagram Generation and SMILES

#include <QtDebug>

#include <map>
#include <sstream>
#include <string>
#include <vector>

#include "atom.h"
#include "defs.h"
#include "dpoint.h"
#include "drawable.h"
#include "molecule.h"
#include "render2d.h"
#include "ring.h"
#include "sdg.h"
#include "setofrings.h"
#include "text.h"

using namespace OpenBabel;

// CleanUp(): Invoke SDG() to clean up structure
void Molecule::CleanUp() { SDG(true); }

// Structure Diagram Generation - add coordinates to connectivity
// generally called after reading in a file or SMILES string which did
// not supply coordinates.  Could also be used to clean up Molecule.
// bool coord: have coordinates been set?  (true = coordinates exist, e.g.
// from file or hand drawing; false = no, strcuture supplied as connection
// table from SMILES or file)
// Method references:
// 1) Ugi I et al., Journal of Chemical research (M), 1991, 2601-2689
// 2) Christoph Steinbeck's Java implentation of above, JMDraw.
//    http://jmdraw.sourceforge.net/
void Molecule::SDG(bool coord) {
    QRect bb1;
    QPoint center1, center2;

    if (coord == true) { // if coordinates exist, save center of bounding box
        bb1 = BoundingBoxAll();
        center1 = bb1.center();
    }
    // DPoint *t2;
    Atom *a1;

    // get unique points
    up = AllPoints();
    QVector<Atom> atoms(up.count());

    // clear "hit" flag on all atoms
    for (DPoint *tmp_pt : up)
        tmp_pt->hit = false;
    // find rings (esp. find aromaticity) - do after CopyTextToDPoint()
    MakeSSSR();
    qInfo() << "Done MakeSSSR()";

    // convert "up" to JMDraw-friendly Qlist<Atom>
    // rebuild neighbors list (usually mangled by MakeSSSR)
    for (DPoint *tmp_pt : up) {
        tmp_pt->neighbors.clear();
        for (Bond *tmp_bond : bonds) {
            if (tmp_bond->Find(tmp_pt) == true) {
                tmp_pt->neighbors.append(tmp_bond->otherPoint(tmp_pt));
                tmp_pt->bondorder[(tmp_pt->neighbors.count() - 1)] = tmp_bond->Order();
            }
        }
    }
    // first copy all DPoints
    int c1, c2, refnum;

    for (c1 = 0; c1 < up.count(); c1++) {
        DPoint *tmp_pt = up.at(c1);
        atoms[c1] = Atom(tmp_pt->element, tmp_pt->x, tmp_pt->y, tmp_pt->z);
        atoms[c1].number = tmp_pt->serial;
        atoms[c1].degree = tmp_pt->neighbors.count();
        qInfo() << c1 << "-degree-" << atoms[c1].degree;
    }
    // now build connectivity table
    for (c1 = 0; c1 < up.count(); c1++) {
        DPoint *tmp_pt = up.at(c1);
        for (c2 = 0; c2 < tmp_pt->neighbors.count(); c2++) {
            refnum = tmp_pt->neighbors.at(c2)->serial;
            atoms[c1].nodeTable.replace(c2, &atoms[refnum]);
            atoms[c1].bondTable[c2] = tmp_pt->bondorder[c2];
            atoms[c1].intnodeTable[c2] = refnum;
        }
    }
    /* convert this_sssr to setOfRings
       setOfRings s1;
       s1.resize(this_sssr.sssr.count());
       Ring *ring1;
       QPtrList<DPoint> *tmp_ring;
       c1 = 0;
       for (tmp_ring = this_sssr.sssr.first(); tmp_ring != 0;
       tmp_ring = this_sssr.sssr.next()) {
       ring1 = new Ring;
       ring1->resize(tmp_ring->count());
       c2 = 0;
       for (tmp_pt = tmp_ring->first(); tmp_pt != 0; tmp_pt = tmp_ring->next())
       { a1 = atoms.at(tmp_pt->serial); ring1->replace(c2, a1); qInfo() << "RA"
       << c2 << ":" << tmp_pt->serial ; c2++;
       }
       ring1->sort2();
       s1.replace(c1, ring1);
       c1++;
       }
     */
    // pass to SDG class.
    class SDG sdg1;
    qInfo() << "SDG starting with atoms: " << atoms.size();

    // TODO Interface with SDG for now, but eventually
    // remove this block after #19 is closed
    QVector<Atom *> sdg_atoms(atoms.size());
    for (int i = 0; i < atoms.size(); ++i) {
        sdg_atoms[i] = &atoms[i];
    }
    // End block

    sdg1.setAtoms(sdg_atoms);
    // sdg1.setRings(s1);
    sdg1.setBL(preferences.getBond_fixedlength());
    sdg1.exec();

    qInfo() << "SDG succeeded!";

    // convert atoms back to DPoint (essentially, just update x,y coordinates)
    for (c1 = 0; c1 < up.count(); c1++) {
        DPoint *tmp_pt = up.at(c1);
        tmp_pt->x = atoms[c1].x;
        tmp_pt->y = atoms[c1].y;
    }

    bb1 = BoundingBoxAll();
    int xmove = 0, ymove = 0;

    if (coord == true) { // if coordinates existed, move back into place
        center2 = bb1.center();
        xmove = center1.x() - center2.x();
        ymove = center1.y() - center2.y();
    } else { // move to top left of screen
        if (bb1.left() < 10)
            xmove = 10 - bb1.left();
        if (bb1.top() < 10)
            ymove = 10 - bb1.top();
    }
    for (DPoint *tmp_pt : up) {
        tmp_pt->x += xmove;
        tmp_pt->y += ymove;
    }

    // add hydrogens
    AddHydrogens();
}

// Use OpenBabel to make InChI strings...
QString Molecule::ToInChI() {
    std::istringstream istream(ToMDLMolfile().toStdString());
    std::ostringstream ostream;

    OBConversion Conv(&istream, &ostream);

    Conv.SetInAndOutFormats("mdl", "InChI");
    Conv.Convert();

    // convert the stream into a terminated c string
    return (QString(ostream.str().c_str()));
}

// Use OpenBabel to make SMILES strings.
QString Molecule::ToSMILES() {
    QString smiles;

    std::istringstream istream(ToMDLMolfile().toStdString());
    std::ostringstream ostream;

    OBConversion Conv(&istream, &ostream);
    Conv.SetInAndOutFormats("mdl", "SMILES");
    Conv.Convert();

    // convert the stream into a terminated c string
    return (QString(ostream.str().c_str()));
}

// convert InChI string to Molecule (using Babel!)
// (Ideally, you should call this function just after creating)
void Molecule::FromInChI(QString sm) {}

// convert InChI or SMILES string to Molecule
// (Ideally, you should call this function just after creating)
void Molecule::FromSMILES(QString sm) {
    qInfo() << "FromSMILES: " << sm;
    QString inputFormat = "smi";
    if (sm.contains("InChI=")) {
        inputFormat = "inchi";
    }
    QByteArray smArray = sm.toLatin1();

    std::istringstream istream(smArray.constData()); // build a stream on the string

    OBMol myMol;
    OBConversion conv;
    OBFormat *format = conv.FindFormat(inputFormat.toLatin1());

    conv.SetInAndOutFormats(format, format);
    myMol.Clear();
    conv.Read(&myMol, &istream);

    // now convert the molecule into XDC's internal representation

    OpenBabel::OBAtom *thisAtom;

    std::vector<DPoint *> avec;

    QString tmp_element, tmp_element_mask;

    DPoint *thisDPoint;

    std::vector<OpenBabel::OBNodeBase *>::iterator ait;

    std::map<OpenBabel::OBAtom *, DPoint *> hashit;
    int i = 0; // this appears to be for debug

    for (thisAtom = myMol.BeginAtom(ait); thisAtom; thisAtom = myMol.NextAtom(ait)) {

        qInfo() << "Adding OBAtom: " << i++ << " of element#: " << thisAtom->GetAtomicNum()
                << " type: " << OBElements::GetSymbol(thisAtom->GetAtomicNum());
        thisDPoint = new DPoint;
        tmp_element = OBElements::GetSymbol(thisAtom->GetAtomicNum());

        tmp_element_mask = tmp_element;
        tmp_element_mask.fill(' '); // fix the mask characters

        thisDPoint->element = tmp_element;
        thisDPoint->elementmask = tmp_element_mask;

        if (tmp_element != "C") {
            Text *nt = new Text(r);

            nt->setPoint(thisDPoint);
            nt->setJustify(JUSTIFY_CENTER);
            nt->Highlight(false);
            nt->setText(tmp_element);
            labels.append(nt);
        }

        avec.push_back(thisDPoint);

        hashit[thisAtom] = thisDPoint;
    }

    OpenBabel::OBBond *thisBond;

    std::vector<OpenBabel::OBBond *>::iterator bit;
    for (thisBond = myMol.BeginBond(bit); thisBond; thisBond = myMol.NextBond(bit)) {
        addBond(hashit[thisBond->GetBeginAtom()], hashit[thisBond->GetEndAtom()], 1,
                thisBond->GetBondOrder(), QColor(0, 0, 0), true);
    }
    qInfo() << "Before SDG, Atoms: " << AllPoints().size() << ", Bonds: " << bonds.size();
    SDG(false); // generate structure coordinates
}

//
// OLD CODE
//
/* convert SMILES string to Molecule
// (Ideally, you should call this function just after creating)
void Molecule::FromSMILES(QString sm) {
  QStringList tokens;
  QPtrStack<DPoint> branch_tree;
  QPtrVector<DPoint> ring_closure_array(10);
  bool ring_array_status[10]; // track which elements of ring array are used

  for (int cc = 0; cc < 10; cc++)
    ring_array_status[cc] = false;

  // tokenize
  // tokens: atoms, groups enclosed in [], (, )
  // note that numbers and symbols outside [] will break the tokenizer
  QStringList smilesTokens;
  QString prev_token, tmp_token;
  int i1;

  qInfo() << "SMILES:" << sm << "|" ;

  do {
    tmp_token = "";
    if (sm[0] == '=') { // double bond
      tmp_token.append("=");
      sm.remove(0,1);
    }
    if (sm[0] == '#') { // triple bond
      tmp_token.append("#");
      sm.remove(0,1);
    }
    if (sm[0].isLetter()) {
      // extract letter token
      // look for single-letter aromatic
      if (sm[0].toLower() == sm[0]) {
        tmp_token.append(sm.left(1));
        sm.remove(0,1);
      } else {
        if ( (sm[1].toLower() == sm[1]) &&
             (sm[1].isLetter()) ) { // lowercase; two-letter symbol
          tmp_token.append(sm.left(2));
          sm.remove(0,2);
        } else { //
          tmp_token.append(sm.left(1));
          sm.remove(0,1);
        }
      }
      // extract ring closure numbers
      if (sm.length() > 0) {
        do {
          if (sm[0].isNumber()) {
            tmp_token.append(sm.left(1));
            sm.remove(0,1);
          } else {
            break;
          }
        } while (sm.length() > 0);
      }
      smilesTokens.append(tmp_token);
    }
    if (sm[0] == '(') {
      tmp_token = "(";
      smilesTokens.append(tmp_token);
      sm.remove(0,1);
    }
    if (sm[0] == ')') {
      tmp_token = ")";
      smilesTokens.append(tmp_token);
      sm.remove(0,1);
    }
    if (sm[0] == '[') {
      i1 = sm.indexOf("]");
      tmp_token = sm.left(i1);
      smilesTokens.append(tmp_token);
    }
    //qInfo() << "token: " << tmp_token << Qt::endl << "left: " << sm ;
  } while (sm.length() > 0);

  DPoint *prev_pt = 0, *new_pt = 0;
  QString tmp_element, tmp_element_mask;
  tmp_token = "";
  prev_token = "";
  int bond_order = 0;
  bool aromatic = false, flag = false;

  for ( QStringList::Iterator it = smilesTokens.begin();
        it != smilesTokens.end();
        ++it ) {
    prev_token = tmp_token;
    tmp_token = (*it).toLatin1();

    qInfo() << "token: " << tmp_token ;

    // process tokens
    if (tmp_token == "(") { // start branch
      branch_tree.push(prev_pt);
      continue;
    }
    if (tmp_token == ")") { // end branch
      prev_pt = branch_tree.pop();
      continue;
    }
    // if not a branch, it's probably an atom
    new_pt = new DPoint;
    // calculate bond order
    aromatic = false;
    if (tmp_token[0].isLetter()) {
      if (tmp_token[0].toLower() == tmp_token[0]) new_pt->aromatic = true;
    }
    if (prev_pt != 0) {
      bond_order = 1;
      if ( prev_pt->aromatic && new_pt->aromatic ) {bond_order = 4;}
    }
    if (tmp_token.left(1) == "=") {
      bond_order = 2;
      tmp_token.remove(0,1);
    }
    if (tmp_token.left(1) == "#") {
      bond_order = 3;
      tmp_token.remove(0,1);
    }
    // extract element info
    tmp_element = ""; tmp_element_mask = "";
    if (tmp_token[0].isLetter()) {
      tmp_element = tmp_token.left(1);
      tmp_element = tmp_element.toUpper();
    }
    if (tmp_token[1].isLetter()) tmp_element.append(tmp_token[1]);
    if (tmp_token[0] == '[') {
      int i1 = tmp_token.indexOf("]");
      tmp_element = tmp_token.mid(1, i1 - 2);
    }
    qInfo() << "element: " << tmp_element ;
    tmp_element_mask = tmp_element;
    tmp_element_mask.fill(' ');
    // add super/sub-script where appropriate

    new_pt->element = tmp_element;
    new_pt->elementmask = tmp_element_mask;
    // create Text where appropriate
    if (new_pt->element != "C") {
      Text *nt = new Text(r);
      nt->setPoint(new_pt);
      nt->setJustify(JUSTIFY_CENTER);
      nt->Highlight(false);
      nt->setText(tmp_element);
      nt->setTextMask(tmp_element_mask);
      labels.append(nt);
    }
    // create bond
    if (prev_pt != 0) {
      addBond(prev_pt, new_pt, 1, bond_order, QColor(0,0,0), true);
      prev_pt = new_pt;
    } else {
      prev_pt = new_pt;
    }
    // handle ring closure
    do {
      flag = false;
      if (tmp_token.at(tmp_token.length() - 1).isNumber()) {
        int ringnum = tmp_token.right(1).toInt();
        flag = true;
        if (ring_array_status[ringnum] == false) { // save this atom
          ring_closure_array.replace(ringnum, new_pt);
          ring_array_status[ringnum] = true;
        } else { // do ring closure
          tmp_pt = ring_closure_array.at(ringnum);
          if (tmp_pt->aromatic && new_pt->aromatic)
            addBond(tmp_pt, new_pt, 1, 4, QColor(0,0,0), true);
          else
            addBond(tmp_pt, new_pt, 1, 1, QColor(0,0,0), true);
          ring_array_status[ringnum] = false;
        }
        tmp_token.remove(tmp_token.length() - 1, 1);
      }
    } while (flag == true);
  }

  // print atom list and connection table?
  qInfo() << ToXML("smiles") ;

  SDG(false);  // generate structure coordinates
}
//
// OLD CODE
//
*/
