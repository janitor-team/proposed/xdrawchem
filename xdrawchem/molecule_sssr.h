// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// molecule_sssr.cpp - defines class SSSR, which implements ring detection by
// breadth-first traversal.  This results in the Smallest Set of Smallest
// Rings.  This algorithm is based on J. Figueras' published method,
// "Ring Perception Using Breadth-First Search",
// J. Chem. Inf. Comput. Sci. 1996, 36, 986-991

#ifndef MOL_SSSR_H
#define MOL_SSSR_H

#include <QList>

#include "dpoint.h"

class RingData {
  public:
    RingData() {
        neighbors = 0;
        bridge = false;
    }
    int neighbors; // number of neighbor rings
    bool bridge;   // bridged ring? (e.g. camphor)
};

class SSSR {
  public:
    QList<QList<DPoint *> *> sssr;
    QList<RingData *> sssr_data;
    DPoint *lastnode; // 'source' node in BFS
    // Build SSSR from these lists
    QList<DPoint *> structureAtoms;
    // BFS queue
    QList<DPoint *> bfs_queue;

    int IsInRing(DPoint *);
    int InSameRing(DPoint *, DPoint *, DPoint *);
    void PrintSSSR();
    void FindAromatic(QList<Bond *>);
    void BuildSSSR(QList<DPoint *>);
    void Add(QList<DPoint *> *);
    int CommonPoints(QList<DPoint *> *, QList<DPoint *> *);
    bool CheckRing(QList<DPoint *> *);
    QList<DPoint *> *GetRing(DPoint *);
    void ClearPaths();
};

#endif
