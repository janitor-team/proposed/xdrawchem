// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <QComboBox>
#include <QDir>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>

#include "defs.h"
#include "ringdialog.h"

// defined in main.cpp
extern QString RingDir;

RingDialog::RingDialog(QWidget *parent) : QDialog(parent) {
    // Init variables which tell which ring was picked
    fn = QString("noring");

    QPixmap pix;
    QLabel *caption;
    QPushButton *dismiss;

    setWindowTitle(tr("Ring tool"));

    QGridLayout *mygrid = new QGridLayout(this);

    caption = new QLabel(tr("Choose a structure from list:"));
    mygrid->addWidget(caption, 0, 0, 0, 4);

    QDir dr(RingDir, "*.cml");
    QStringList dl = dr.entryList();

    filelist = new QComboBox();
    filelist->addItem("(pick)");
    for (int cc = 0; cc < dl.count(); cc++)
        filelist->addItem(dl[cc].left(dl[cc].length() - 4));
    connect(filelist, SIGNAL(activated(int)), this, SLOT(fromComboBox(int)));

    mygrid->addWidget(filelist, 1, 1, 0, 4);

    dismiss = new QPushButton(tr("Cancel"));
    connect(dismiss, SIGNAL(clicked()), SLOT(reject()));
    mygrid->addWidget(dismiss, 2, 2, 1, 3);
}

void RingDialog::fromComboBox(int i) {
    if (filelist->currentText() == "(pick)")
        return;
    fn = filelist->currentText() + QString(".cml");
    // qDebug() << "File: " << fn ;
    accept();
}

// cmake#include "ringdialog.moc"
