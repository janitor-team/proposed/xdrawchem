// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// symbol.h -- subclass of Drawable for symbols

#ifndef SYMBOL_H
#define SYMBOL_H

#include "dpoint.h"
#include "drawable.h"
#include "render2d.h"

class Symbol : public Drawable {
  public:
    Symbol(Render2D *, QObject *parent = 0);
    Symbol *CloneTo(Drawable *target = nullptr) const;
    void Render(); // draw this object
    void Edit();
    int Type();          // return type of object
    bool Find(DPoint *); // does this ymbol contain this DPoint?
    DPoint *FindNearestPoint(DPoint *, double &);
    Drawable *FindNearestObject(DPoint *, double &);
    void setPoint(DPoint *);
    QRect BoundingBox();
    bool isWithinRect(QRect, bool);
    bool WithinBounds(DPoint *);
    QString ToXML(QString);
    void FromXML(QString);
    void SetSymbol(QString);
    QString GetSymbol() { return which; }
    void SetOffset(QPoint d) {
        if ((d.x() == 0) && (d.y() == 0)) {
            need_offset = false;
            offset = d;
        } else {
            need_offset = true;
            offset = d;
        }
    }
    QPoint GetOffset() { return offset; }
    void SetRotate(double);
    double GetRotate() { return rotation; }

  private:
    // Renderer
    Render2D *m_renderer;
    // Pixmap of original and rotated, regular and highlighted symbol
    QPixmap originalRegular;
    QPixmap originalHighlighted;
    QPixmap rotateRegular;
    QPixmap rotateHighlighted;
    // Offset (if needed -- to avoid label or bond)
    bool need_offset;
    QPoint offset;
    // Rotation (if needed -- if not below or on point)
    double rotation;
    // scale (if needed)
    double scale;
};

#endif
