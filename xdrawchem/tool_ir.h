// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef TOOL_IR_H
#define TOOL_IR_H

#include <QTextEdit>

#include "molecule.h"
#include "ngw.h"
#include "tooldialog.h"

using namespace OpenBabel;

class Tool_IR_Dialog : public ToolDialog {
  public:
    Tool_IR_Dialog(QWidget *parent = 0);
    void process();

  private:
    NewGraphWidget *ngw;
    QTextEdit *peaktext;
};

#endif
