// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// XML_Reader.h - base class for XML type readers

class XML_Reader {
  public:
    // reimplement for subclasses
    virtual bool readFile(const QString &) { return false; }
    // utility functions
    bool selfContainedTag(const QString &);
    QString readTag(const QString &, int);
    QString readData(const QString &, int);
    QStringList readAttr(const QString &);
    QStringList tokenize(const QString &);
    void parseBoundingBox(const QString &, double, double, double, double);
    int positionOfEndTag(const QString &, const QString &);
};
